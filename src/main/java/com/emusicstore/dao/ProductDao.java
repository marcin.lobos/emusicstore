package com.emusicstore.dao;

import com.emusicstore.model.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductDao {

    private List<Product> productList;

    public List<Product> getProductList() {

        Product product1 = new Product();
        Product product2 = new Product();
        Product product3 = new Product();

        product1.setProductId("1");
        product1.setProductName("gitara");
        product1.setProductCategory("instrument");
        product1.setProductDesctription("to jest gitara drewniana");
        product1.setProductDesctription("to jest gitara drewniana");
        product1.setProductPrice(205);
        product1.setProductCondition("new");
        product1.setProductStatus("Active");
        product1.setUnitInStock(11);
        product1.setProductManufacturer("Alaskan instruments");

        product2.setProductId("2");
        product2.setProductName("flet");
        product2.setProductCategory("instrument");
        product2.setProductDesctription("to jest flet");
        product2.setProductPrice(222);
        product2.setProductCondition("new");
        product2.setProductStatus("Active");
        product2.setUnitInStock(11);
        product2.setProductManufacturer("Flets industry");

        product3.setProductId("3");
        product3.setProductName("piano");
        product3.setProductCategory("instrument");
        product3.setProductDesctription("black piano");
        product3.setProductPrice(233);
        product3.setProductCondition("new");
        product3.setProductStatus("Active");
        product3.setUnitInStock(11);
        product3.setProductManufacturer("Chopin pianos");

        productList = new ArrayList<>();
        productList.add(product1);
        productList.add(product2);
        productList.add(product3);
        return productList;
    }

    public Product getProductById(String idProduct) throws IOException {

        for (Product p : productList) {
            if (p.getProductId().equals(idProduct)) {
                return p;
            }
        }
        throw new IOException("no product found");
    }
}
