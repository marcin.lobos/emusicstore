package com.emusicstore.controller;

import com.emusicstore.dao.ProductDao;
import com.emusicstore.model.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

@Controller
public class HomeController {

    private ProductDao productDao = new ProductDao();

    @RequestMapping("/")
    public String home() {
        return "home";
    }

    @RequestMapping("/productList")
    public String getProducts(Model model) {

        List<Product> productList = productDao.getProductList();

        model.addAttribute("products", productList);
        return "products";
    }

    @RequestMapping("/productList/product/{Id}")
    public String getProductDetails(Model model,  @PathVariable String Id) throws IOException {
        Product chosenProduct = productDao.getProductById(Id);
        model.addAttribute("product", chosenProduct);
        return "viewproduct";
    }

}
