<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 2019-11-02
  Time: 19:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<html>

<%@include file="template/header.jsp"%>


<div class="container-wrapper">

    <!-- FOOTER -->
    <div class="container">
        <div class="page-header">
            <h1>All products</h1>
            <p class="lead">Checkout all products.</p>
        </div>

        <table class="table table-striped table-hover">
            <thead class="">
            <tr>
                <th> Photo</th>
                <th> Name</th>
                <th> Category</th>
                <th> Condition</th>
                <th> Price</th>
                <th> Product page</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="product" items="${products}">
                <tr>
                    <td><img src="#" alt="foto"/></td>
                    <td> ${product.productName}</td>
                    <td> ${product.productCategory}</td>
                    <td> ${product.productCondition}</td>
                    <td> ${product.productPrice} USD</td>
                    <td> <a href="<c:url value="/productList/product/${product.productId}" />"> <span class="glyphicon glyphicon-info-sign"> info </span> </a> </td>
                </tr>
            </c:forEach>
            </tbody>

        </table>


  <%@include file="template/footer.jsp"%>
