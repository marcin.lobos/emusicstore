<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<html>

<%@include file="template/header.jsp" %>


<div class="container-wrapper">


    <div class="container">
        <div class="page-header">
            <h1>Product detail</h1>
            <p class="lead">detail of product.</p>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img src="#" alt="image" style="width: 100%; height: 300px">
                </div>
                <div class="col-md-5">
                    <h3>Name: ${product.productName} </h3>
                    <p> Description: ${product.productDesctription}</p>
                    <p><strong>Manufactuer:</strong> ${product.productManufacturer}</p>
                    <p><strong>Category:</strong> ${product.productCategory}</p>
                    <p><strong>Condition:</strong> ${product.productCondition}</p>
                    <p> Price: ${product.productPrice} USD</p>
                </div>
            </div>
        </div>


        <%@include file="template/footer.jsp" %>
